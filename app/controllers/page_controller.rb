class PageController < ApplicationController
  before_action :require_logged_out, except: [:admin]
  before_action :require_admin, only: [:admin]
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def record_not_found
    flash[:notice] = 'Error forced a user logout'
    redirect_to logout_path
  end

  def index
    @sale = Sale.all
  end
end
